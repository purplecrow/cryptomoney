import React from 'react';
import logo from './logo.svg';
import './App.css';
import Sidebar from './components/sidebar/sidebar';
import Appbar from './components/Appbar/Appbar';
import Menus from  './components/Menus/Menus';
import Charts from './components/Charts/Charts';
import OrderBook from './containers/OrderBook/OrderBook';
function App() {
  return (
    <div className="App">
        {/* <Sidebar /> */}
        {/* <Appbar /> */}
        <Menus />
        <Charts />
        <OrderBook />

    </div>
  );
}

export default App;
