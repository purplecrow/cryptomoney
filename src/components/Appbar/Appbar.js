import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import  {AppBar,Toolbar,Typography} from '@material-ui/core';
import {AccountCircle}  from  '@material-ui/icons';



const styles = {

};


function Appbar(props) {
    const {classes} =  props;
    return (
        <div>
            <AppBar position="static" color="default">
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        Photos
                    </Typography>
                    <AccountCircle />
                </Toolbar>
            </AppBar>
            
        </div>
    )
}

export default withStyles(styles)(Appbar);
