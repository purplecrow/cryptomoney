import React,{useState} from 'react'
import {SwipeableDrawer,ListItem,List} from '@material-ui/core';


function Sidebar() {

    const [sidebarDisplayItems,setSidebarDisplayItems] = useState(['a','b','c'])

    return (
        <div>
            {/* <p>ok</p>  */}
            <SwipeableDrawer anchor = 'left' open={true}>
                {
                    <List>
                        {
                        sidebarDisplayItems.map((elem)=>{
                            return (<ListItem>
                                        <p>elem</p>
                                    </ListItem>)
                        })
                        }
                    </List>     

                }
            </SwipeableDrawer>      
        </div>
    )
}

export default Sidebar
