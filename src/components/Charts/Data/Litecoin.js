import React, { Component } from 'react';
import { Fragment } from 'react';


function mapStateToProps(state) {
    return {

    };
}

function Iframe(props) {
    return (<div dangerouslySetInnerHTML={ {__html:  props.iframe?props.iframe:""}} />);
  }


class Litecoin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chart_socket: ''
        }
    }  

    componentDidMount(){
        
    }



    render() {
        let iframe;
        if(window.innerWidth > 400){
            iframe = '<iframe height="530px;" style="width: 42vw;" scrolling="no" title="fx." src="https://charts.biviolin.com/litecoin.html" frameborder="no" allowtransparency="true" allowfullscreen="true">See the Pen <a href="https://codepen.io/ycw/pen/JqwbQw/">fx.</a> by ycw(<a href="https://codepen.io/ycw">@ycw</a>) on <a href="https://codepen.io">CodePen</a>.</iframe>'; 
        }else{
            iframe = '<iframe height="330px;" style="width: 80vw;" scrolling="no" title="fx." src="https://charts.biviolin.com/litecoin.html" frameborder="no" allowtransparency="true" allowfullscreen="true">See the Pen <a href="https://codepen.io/ycw/pen/JqwbQw/">fx.</a> by ycw(<a href="https://codepen.io/ycw">@ycw</a>) on <a href="https://codepen.io">CodePen</a>.</iframe>'; 
        }

        return (
            <Fragment>
                <Iframe iframe={iframe} />          
            </Fragment>)
    }
}

export default Litecoin;
