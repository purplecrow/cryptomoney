import React, { Component } from 'react';
import socketIOClient from 'socket.io-client';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { RoomRounded } from '@material-ui/icons';
import Grid from '@material-ui/core/Grid';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import Box from '@material-ui/core/Box';

export default class OrderBook extends Component {

    constructor(props) {
        super(props)

        this.state = {
            orderBookData :{
                'BTC' : {
                    'BUY' : [],
                    'SELL' : [],
                    'RECENT' : []
                },
                'ETH' : {
                    'BUY' : [],
                    'SELL' : [],
                    'RECENT' : []
                },
                'LTC' : {
                    'BUY' : [],
                    'SELL' : [],
                    'RECENT' : []
                },
                'XRP' : {
                    'BUY' : [],
                    'SELL' : [],
                    'RECENT' : []
                }
            },
            'currency_selection' : 'BTC',
        }
    }

    componentDidMount() {
        const socketOrder = socketIOClient('https://socket.biviolin.com/');
        socketOrder.on(`BTC-ORDER-BOOK`,(pl)=>{
            this.setState({
                orderBookData: {...this.state.orderBookData , 'BTC' : pl}
            })
        })

        const socketOrder1 = socketIOClient('https://socket.biviolin.com/');
        socketOrder1.on(`ETH-ORDER-BOOK`,(pl)=>{
            console.log('ethereum order book');
            console.log(pl);
            this.setState({
                orderBookData: {...this.state.orderBookData , 'ETH' : pl}
            })
        })

        const socketOrder2 = socketIOClient('https://socket.biviolin.com/');
        socketOrder2.on(`XRP-ORDER-BOOK`,(pl)=>{
            this.setState({
                orderBookData: {...this.state.orderBookData  , 'XRP' : pl}
            })
        })

        const socketOrder3 = socketIOClient('https://socket.biviolin.com/');
        socketOrder3.on(`LTC-ORDER-BOOK`,(pl)=>{
            this.setState({
                orderBookData: {...this.state.orderBookData  , 'LTC' : pl}
            })
        })
        const socket = socketIOClient('https://socket.biviolin.com/');
        socket.on(`CHART_BTC`,(pl)=>{
            if(this.state.currency_selection === 'BTC'){
                this.setState({
                    closeValue: pl.close
                })
            }
        })

        socket.on(`CHART_ETH`,(pl)=>{
            if(this.state.currency_selection ==='ETH'){
                this.setState({
                    closeValue: pl.close
                })
            }
            
        })

        socket.on(`CHART_LTC`,(pl)=>{
            if(this.state.currency_selection === 'LTC'){
                this.setState({
                    closeValue: pl.close
                })
            }
            
        })

        socket.on(`CHART_XRP`,(pl)=>{
            if(this.state.currency_selection === 'XRP'){
                this.setState({
                    closeValue: pl.close
                })
            }
            
        })
    }

    render() {

        const tableStyle=  {
            maxWidth: '500px',
            border : '1px solid black',
            margin:'10px'
        };

        const paperStyle = {
    
            padding: '2px',
            textAlign: 'center',
            color: 'red',
    }
        

        return (
            <div>

                <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <p><b>Bitcoin (BTC/USD)  - OrderBook</b></p>
                            <TableContainer component={Paper}>
                                <Table style={tableStyle} aria-label="simple table">
                                    <TableHead>
                                    <TableRow>
                                        <TableCell>Price</TableCell>
                                        <TableCell align="right">Quantity</TableCell>
                                    </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {this.state.orderBookData && this.state.orderBookData[this.state.currency_selection].BUY.map((row,i) => (
                                        <TableRow key={i}>
                                            <TableCell component="th" scope="row" >
                                                <Box display="flex" flexDirection="row"  bgcolor="inherit">
                                                    <Box  bgcolor="inherit">
                                                        <ArrowUpwardIcon  style={{color:'green'}}/>
                                                    </Box>
                                                    <Box  bgcolor="inherit">
                                                        {Number(parseFloat(row.price).toFixed(4))}
                                                    </Box>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="right">{row.quantity}</TableCell>
                                        </TableRow>
                                    ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item xs={4}>
                            <p><b>Bitcoin (BTC/USD)  - OrderBook</b></p>
                            <TableContainer component={Paper}>
                                <Table style={tableStyle} aria-label="simple table">
                                    <TableHead>
                                    <TableRow>
                                        <TableCell>Price</TableCell>
                                        <TableCell align="right">Quantity</TableCell>
                                    </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {this.state.orderBookData && this.state.orderBookData[this.state.currency_selection].BUY.map((row,i) => (
                                        <TableRow key={i}>
                                            <TableCell component="th" scope="row">
                                                <Box display="flex" flexDirection="row"  bgcolor="inherit">
                                                    <Box  bgcolor="inherit">
                                                        <ArrowDownwardIcon  style={{color:'red'}}/>
                                                    </Box>
                                                    <Box  bgcolor="inherit">
                                                        {Number(parseFloat(row.price).toFixed(4))}
                                                    </Box>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="right">{Number(parseFloat(row.price).toFixed(4))}</TableCell>
                                        </TableRow>
                                    ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid> <Grid item xs={4}>
                            <p><b>Bitcoin (BTC/USD)  - OrderBook</b></p>
                            <TableContainer component={Paper}>
                                <Table style={tableStyle} aria-label="simple table">
                                    <TableHead>
                                    <TableRow>
                                        <TableCell>Price</TableCell>
                                        <TableCell align="right">Quantity</TableCell>
                                    </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {this.state.orderBookData && this.state.orderBookData[this.state.currency_selection].BUY.map((row,i) => (
                                          <TableRow key={i}>
                                          <TableCell component="th" scope="row">
                                              <Box display="flex" flexDirection="row"  bgcolor="inherit">
                                                  <Box  bgcolor="inherit">
                                                      <ArrowDownwardIcon  style={{color:'red'}}/>
                                                  </Box>
                                                  <Box  bgcolor="inherit">
                                                      {Number(parseFloat(row.price).toFixed(4))}
                                                  </Box>
                                              </Box>
                                          </TableCell>
                                          <TableCell align="right">{Number(parseFloat(row.price).toFixed(4))}</TableCell>
                                      </TableRow>
                                    ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                </Grid>
               
            </div>
        )
    }
}
